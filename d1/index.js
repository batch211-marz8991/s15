console.log("Hello World");

//JavaScript renders web pages in an interactive and dynamic fashion. Meaning, it enables us to create dynamically updating content, control multimedia, and animate images

//Syntax, Statements, and Comments

//Statements:

//Statements in programming are instructions that we tell the computer to perform
//JS statements usually end with a semicolon (;)
//Semicolons are not required in JS, but we will use it to help us train to locate where a statement ends

//Syntax in programming, it is the set of rules that descrives how statements must be constructed

//Comments are parts of the code that gets ignored by the language
//Comments are meant to describe the written code


/*
	There are two types of comments:
	1. The single-line comment (ctril + /)
	**denoted by two slahes

	2. The multi-line comment (ctrl + shift + /)
	**denoted by a slash and asterisk

*/

//Variables

//It is used to contain data
// Any information that is used by an application is stored in what we call a "memory"
// When we create variables, certain portions of a device's memory is given a "name" that we call "variables"
//This makes it easier for us to associate information stored in our devices to actual "names" about infromation

//Declaring variables
//declaring variables - tells our devices that a variable name is created and is ready to store data

let myVariable;
console.log(myVariable);

//Declaring a variable without giving it a value will automatically assign it with the value of "undefined", meaning the variable's value was "not defined".

//Syntax
	// let/const variableName;

//console.log() is useful for printing values of variables or certain results of code into Google Chrome browser's console
//constant use of this throughout developing an application will save us time and builds good habits in always checking for the output of our code.

let hello;
console.log(hello);

//Variables must be declared first before they are used
//using variables before they are declared will retun an error

/*
	Guides in writing variables
		1. Use the 'let' keyword followed by the variable name of your choosing and use the assignment operator (=) to assign a value
		2. Variable names should start with a lowercase character, use camelCase for multiple words
		3. For constant variables, use the 'const' keyword
		4. Variable names should be indicative (or descriptive) of the value being stored to avoid confusion.
*/

//Declare and initialize variables
	//Initializing variables - the instance when a variable is given its initial or startin value
	//Syntax
		//let/const variableName = value;

		let productName = 'desktop computer';
		console.log(productName);

		let productPrice = 18999;
		console.log(productPrice);

	// In the context of certain applications, some variables/information are constant and should not be changed
	// In this example, the interest rate for a loan, savings account or a mortgage must not be changed due to real world concerns
	// This is the best way to prevent applications from suddenly breaking or performing in ways that are not intended


		const interest = 3.539;
		// const pi = 3.1416;

		//Reassigning variable values
		//reassigning a variable means changing its intial or previous value into another value
		//Syntax
			//variableName = newValue;
		productName = 'Laptop';
		console.log(productName);

		// console.log(interest);
		// console.log(pi);


			// let variable cannot be re-declared within its scope so this will work:
		// let friend = 'Kate';
		// friend ='Nej';
		// console.log(friend);


		// let friend = 'Kate';
		// let friend ='Nej';// error
		// console.log(friend);
		// could not declare twice

		// interest = 4.489; // error


// reassigning variables vs initializing variables
// declare a variable first

		let supplier;
		supplier = "John Smith Trading";
		// initialization
		// this is considered as initialization because it's the fist time that the value assigned
		
		console.log(supplier)
		// this is cosidered as reassignment coz its value already declared
		supplier = "Zuitt store";
		console.log(supplier)

		// we cannot declare constant variable without initialization
		// const pi ;
		// pi = 3.1416;
		// console.log(pi)//error

		// var 
		// var is also used in declaring a variable
		// let/const they were introduced as new

		a = 5;
		console.log(a) //output: 5
		var a;


		// hoisting is javascript's default behavior of moving declaration to the top
		//in terms of variables anfd constants, key var is hoisted and let  and const does not allow hoisting




		//le/const local/global scope
			// scope essentially means where these variable are available for use
			//let and const are block-scope
			//block is a chunk of code bounded by {}. a block live in curly braces. anything in the curly braces are block


			let outerVariable ="Hello";

			{
				let innerVariable ="Hello again"
			}


			console.log(outerVariable);
			// console.log(innerVariable);//inner variable is not defined


			//multiple variable declaration
			//multiple variable in 1 line
			// let productCode ='DC017' productBrand = 'Dell';
			let productCode ='DC017';
			const productBrand = 'Dell';
			console.log(productCode, productBrand); //DC017 Dell

			//using a variable with reverse keyword
			// const let = "hello";
			// console.log(let); //error

			//data types

			//Strings ('') or ("")
			//strings are a series of characters that create a word, phrase, a sentence or anything related to creating text


			let country = 'Philippines';
			let province = "Metro Mania"


			//concatenating Strings
			//multiple string values can be combined to create a single string using + symbol

			let fullAdress = province + ', ' + country;
			console.log(fullAdress);

			let greeting = "i live in the" + country;


			//the escape character(\) in strings in combination with other character can produce different effects
			// "\n" refers to creating a new line between

			let mailAddress = 'Metr Manila\n\nPhilippines';
			console.log(mailAddress)

			//double qoutes with single qoute allow us to easily include single qoutes in text using the escape character

			let message = "john's employees went home early";
			console.log(message);
			message = 'john\'s employees went home early';
			console.log(message);

			//Numbers

			//Integers/whole numbers

			let headcount = 26;
			console.log(headcount)

			//decimal/fractions

			let grade = 98.7;
			console.log(grade);


			//exponent
			let planetDistance = 2e10;
			console.log(planetDistance);


			//combine text and strings
			console.log("john'sgrade last quarter is " + grade);


			//Boolean
			//boolean values are use to store values related to thestate of certain things

			let isMarried = false;
			let inGoodConduct = true;

			console.log("is Married:" + isMarried)
			console.log("isGoodConduct;" + inGoodConduct);


			//Arrays
			//special kind of datatype use to store multiple value

				//array literals []
					let grades = [98.7, 92.1, 90.2, 94.6]
					console.log(grades);
				
					//differnt data types
					//storing different data types inside an array is not recommended

					// let details = ["John", "smith"]


					//objects
					//a data type that mimic a real world objects items
					// used to create complex data that contains pieces of information

					//syntax
					//let/const objectName ={
						//propertyA : value,
						//propertyB : value
					//}



					let myGrades = {
						firstGrading: 98.7,
						secondGrading: 78.7,
						thirdGrading: 68.7,
						fourthGrading: 58.7,
					}
					console.log(myGrades);

			//typeof operator

			console.log(typeof myGrades);//object
			console.log(typeof grades);


			const anime = ['OP', 'OPM', 'AOT', 'BNHA'];

			anime[0] = 'JJK';
			console.log(anime);

			//null
			//express absence of value

			let spouse = null;
			console.log(spouse)
			//null also considered as data type


			let myNumber = 0;
			let myString = '';
			console.log(myNumber)
			console.log(myString)


			//undefined
			//declared variable without assigned value

			let fullName;
			console.log(fullName)//undifine


			//undifine is a variable created but no value assigned
			//null has a value of zero.
